/*
 * La clase Sprite hará de clase madre
 * de las clases Nave, Marciano y Disparo
 *
 */
package codigo;

import java.awt.Image;

/**
 *
 * @author joseignacionavassanz
 */
public abstract class Sprite {
    
    //Declaramos las imágenes
    public Image imagen = null;
    public Image imagen2 = null;
    
    //Posición X
    private int x = 0;
    //Posición Y
    private int y = 0;
    
    //Velocidad
    private int velocidad = 3;
    
    private int velocidadCambio;
    
    //Método mueve
    abstract public void mueve();
    
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }

    public int getVelocidadCambio() {
        return velocidadCambio;
    }

    public void setVelocidadCambio(int velocidadCambio) {
        this.velocidadCambio = velocidadCambio;
    }
   
}
