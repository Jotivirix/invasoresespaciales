/*
 * Clase Marciano
 * En esta clase añadiremos los marcianos
 * del juego a los cuales iremos eliminando
 * cuando les impactemos con las balas
 */
package codigo;

import java.awt.Image;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 * AUTOR: José Ignacio Navas Sanz
 *
 * CURSO: 1º DAM
 *
 * ASIGNATURA: Programación para Lenguajes Estructurados
 */
public class Marciano extends Sprite{
    
    //CONSTRUCTOR DEL DISPARO
    public Marciano() {
        this.setVelocidad(1);
        try {
            imagen = ImageIO.read(getClass().getResource("/imagenes/marcianito1.png"));
            imagen2 = ImageIO.read(getClass().getResource("/imagenes/marcianito2.png"));
            //this.altoMundo = _altoMundo;
        } catch (IOException e) {
            
        }
    }
    
    //Método mueve
    public void mueve(boolean _direccionMarcianos){
        
        if(_direccionMarcianos){
            this.setX(this.getX() - this.getVelocidad());
        }
        if (!_direccionMarcianos) {
            this.setX(this.getX() + this.getVelocidad());
        }
        
    }

    @Override
    public void mueve() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
