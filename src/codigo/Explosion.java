/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author joseignacionavassanz
 */
public class Explosion extends Sprite{
    
    //Temporizador para las imágenes de la explosión
    private int tiempoDeVida = 20;
    
    public Explosion(){
        this.setVelocidad(2);
        try {
            imagen = ImageIO.read(getClass().getResource("/imagenes/e1.png"));
            imagen2 = ImageIO.read(getClass().getResource("/imagenes/e2.png"));
        } catch (IOException e) {
            
        }
    }
    
    @Override
    public void mueve() {

    }

    public int getTiempoDeVida() {
        return tiempoDeVida;
    }

    public void setTiempoDeVida(int tiempoDeVida) {
        this.tiempoDeVida = tiempoDeVida;
    }
    
}
