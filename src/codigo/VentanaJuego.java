/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.Timer;

/**
 * INVASORES ESPACIALES Recreación mítica del videojuego de marcianitos llamado
 * "Space Invaders"
 *
 * AUTOR: José Ignacio Navas Sanz
 *
 * CURSO: 1º DAM
 *
 * ASIGNATURA: Programación para Lenguajes Estructurados
 */
public class VentanaJuego extends javax.swing.JFrame {

    static int ANCHOPANTALLA = 600;
    static int ALTOPANTALLA = 700;

    /*Bucle de Animación del Juego*/
 /*
    Vamos a crear una escena, con los marcianos
    en una posición y pasada una serie de milisegundos
    moveremos los marcianos hacia un lado u otro. Al 
    hacerlo así, simplemente es como si estuviésemos
    metiendo imágenes fijas y pareciendo así que los
    marcianos se están moviendo
    
    Al utilizar un bucle de animació no colapsamos la
    máquina. Es como si creásemos un hilo de ejecución
    en segundo plano y actualizásemos ese hilo y luego
    lo pasemos al hilo principal del juego permitiendo 
    así que no se colapse la máquina
     */
    //Variables de instancia para el juego
    //Buffer    
    BufferedImage buffer = null;

    //Añadimos un temporizador para ese hilo de ejecución
    Timer temporizador = new Timer(10, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            bucleDelJuego();
        }
    });

    //Declaro la nave
    Nave miNave = new Nave(ANCHOPANTALLA);

    //Decalro los ArrayList
    ArrayList<Disparo> listaDisparos = new ArrayList();

    ArrayList<Marciano> listaMarcianos = new ArrayList();
    
    ArrayList<Explosion> listaExplosiones = new ArrayList();

    //Variable booleana que gobierna el movimiento de los marcianos
    boolean direccionMarcianos = false;
    
    //Contador para el bucle de animacion
    int contador = 0;

    boolean cambiaImagen = false;
    
    //Declaro la velocidad de la nave
    int velocidad = 3;
    
    //Declaro una puntuación inicial
    int puntuacion = 0;
    
    //Declaro puntuación final
    int puntuacionMaxima = 5000;

    /**
     * Creates new form VentanaJuego
     */
    public VentanaJuego() {
        initComponents();
        
        //Establecemos el tamaño de la pantalla
        this.setSize(ANCHOPANTALLA, ALTOPANTALLA); //this. opcional

        //Creamos los marcianos y los añadimos a la lista
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 10; j++) {
                Marciano miMarciano = new Marciano();
                miMarciano.setX(j * (15 + miMarciano.imagen.getWidth(null)));
                miMarciano.setY(i * (10 + miMarciano.imagen.getHeight(null)));
                listaMarcianos.add(miMarciano);
            }
        }

        //Creamos el buffer con el tmaño del jPanel1
        buffer = (BufferedImage) jPanel1.createImage(ANCHOPANTALLA, ALTOPANTALLA);
        //Inicializamos la parte gráfica del buffer
        buffer.createGraphics();
        
        //Definimos las posiciones X e Y de la nave
        miNave.setX(ANCHOPANTALLA / 2);
        miNave.setY(ALTOPANTALLA - 60);
        //Arrancamos el temporizador
        temporizador.start();        
    }

    //Método pintaDisparos
    private void pintaDisparos(Graphics2D g2) {
        for (int i = 0; i < listaDisparos.size(); i++) {
            Disparo aux;
            aux = listaDisparos.get(i);
            //Recoloco los disparos
            aux.mueve();
            /*
            Si la posicion Y del disparo es menor
            o igual a 0 es que ha llegado al techo
            y por lo tanto, debo borrar el disparo
            Si no, sigo pintando el disparo
             */
            if (aux.getY() <= 0) {
                listaDisparos.remove(aux);
            } else {
            }
            //Pinto los disparos
            g2.drawImage(aux.imagen, aux.getX(), aux.getY(), null);
        }
    }

    //Método pintaMarcianos
    private void pintaMarcianos(Graphics2D g2) {
        boolean cambia = false;
        int puntuacionCambio = 2000;
        for (int i = 0; i < listaMarcianos.size(); i++) {
            Marciano aux;
            aux = listaMarcianos.get(i);
            //Recoloco los marcianos
            aux.mueve(direccionMarcianos);
            
            if (aux.getX() + aux.imagen.getWidth(null) > ANCHOPANTALLA) {
                cambia = true;
            }
            if (aux.getX() <= 0) {
                cambia = true;
            }
            //Pinto los marcianos
            //Falta el sistema para saber que imagen pintar
            if (cambiaImagen) {
                g2.drawImage(aux.imagen, aux.getX(), aux.getY(), null);
            } else {
                g2.drawImage(aux.imagen2, aux.getX(), aux.getY(), null);
            }
        }
        //Si es true, es que algún marciano ha tocado la pared derecha
        if (cambia) {
            //Nueva manera de leer un ArrayList
            for (Marciano aux : listaMarcianos) {
                aux.setY(aux.getY() + aux.imagen.getHeight(null) / 2);
            }
            if (direccionMarcianos) {
                direccionMarcianos = false;
            } else {
                direccionMarcianos = true;
            }
            //direccionMarcianos = direccionMarcianos? false:true;
        }
    }

    //Método chequeaColision
    private void chequeaColision() {
        //Creo un rectangulo para guardar el borde de la imagen del marciano
        Rectangle2D.Double rectanguloMarciano = new Rectangle2D.Double();

        //Creo un rectangulo para guardar el borde de la imagen del disparo
        Rectangle2D.Double rectanguloDisparo = new Rectangle2D.Double();

        for (int j = 0; j < listaDisparos.size(); j++) {
            Disparo d = listaDisparos.get(j);
            //Asigno el marco a la posición del disparo
            rectanguloDisparo.setFrame(d.getX(), d.getY(), d.imagen.getWidth(null), d.imagen.getHeight(null));
            for (int i = 0; i < listaMarcianos.size(); i++) {
                Marciano m = listaMarcianos.get(i);
                //Asigno el marco a la posición del marciano
                rectanguloMarciano.setFrame(m.getX(),m.getY(),m.imagen2.getWidth(null), m.imagen2.getHeight(null));
                if (rectanguloDisparo.intersects(rectanguloMarciano)) {
                    //Creamos la explosión
                    Explosion e = new Explosion();
                    //Añadimos las explosiones en la misma posición del marciano
                    e.setX(m.getX());
                    e.setY(m.getY());
                    listaExplosiones.add(e);
                    //Elimino al marciano y al disparo
                    listaMarcianos.remove(m);
                    listaDisparos.remove(d);
                    puntuacion=puntuacion+100;
                }
            }
        }
    }
     
    private void pintaExplosiones(Graphics2D g2){
        for (int i = 0; i < listaExplosiones.size(); i++) {
            Explosion e = listaExplosiones.get(i);
            //Resto 1 de vida a la explosión
            e.setTiempoDeVida(e.getTiempoDeVida()-1);
            
            //Pinto la explosión
            if (e.getTiempoDeVida() > 10) {
                g2.drawImage(e.imagen, e.getX(), e.getY(), null);
            } 
            else if(e.getTiempoDeVida() > 0) {
                g2.drawImage(e.imagen2, e.getX(), e.getY(), null);
            }
            //La explosion ha terminado
            else {
                listaExplosiones.remove(e);
            }
        }
        
    }

    //Declaramos el método bucleDelJuego
    private void bucleDelJuego() {
        contador++;
        if (contador % 30 == 0) {
            if (cambiaImagen) {
                cambiaImagen = false;
            } else {
                cambiaImagen = true;
            }
        }
        //Apuntamos al buffer
        Graphics2D g2 = (Graphics2D) buffer.getGraphics();
        //Pinto un rectángulo que hará de fondo (Color NEGRO Tamaño JPANEL)
        g2.setColor(Color.black);
        g2.fillRect(0, 0, ANCHOPANTALLA, ALTOPANTALLA);
        ///////////////////////////////////////////////////////////////////
        
        //Pinto los disparos               
        pintaDisparos(g2);

        //Pinto los marcianos
        pintaMarcianos(g2);
        
        //Comprobamos las colisiones
        chequeaColision();
        
        //Pinto las explosiones
        pintaExplosiones(g2);

        //Muevo la nave
        miNave.mueve();

        //Añadimos la nave
        g2.drawImage(miNave.imagen, miNave.getX(), miNave.getY(), null);

        ///////////////////////////////////////////////////////////////////
        //Apunto al jPanel y dibujo el buffer sobre el jPanel
        g2 = (Graphics2D) jPanel1.getGraphics();
        g2.drawImage(buffer, 0, 0, null);
        
        

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        /*
        Método que hace que la nave se mueva
         */
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_LEFT: {
                miNave.setPulsadoIzquierda(true);
            }
            break;
            case KeyEvent.VK_RIGHT: {
                miNave.setPulsadoDerecha(true);
            }
            break;
            case KeyEvent.VK_SPACE: {
                //Creo el disparo y lo añado al ArrayList
                Disparo d = new Disparo();
                d.posicionaDisparo(miNave);
                listaDisparos.add(d);
            }
            break;
        }
    }//GEN-LAST:event_formKeyPressed

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        /*
        Método para detener la nave al soltar la tecla
        que estemos pulsando en ese momento (IZQ o DER)
         */
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_LEFT: {
                miNave.setPulsadoIzquierda(false);
            }
            break;
            case KeyEvent.VK_RIGHT: {
                miNave.setPulsadoDerecha(false);
            }
            break;
        }
    }//GEN-LAST:event_formKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaJuego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaJuego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaJuego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaJuego.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaJuego().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
