/*
 * Clase Disparo
 * En esta clase implementaremos los
 * disparos que realizará la nave del
 * juego para destruir a sus rivales
 */
package codigo;

import java.awt.Image;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * AUTOR: José Ignacio Navas Sanz
 *
 * CURSO: 1º DAM
 *
 * ASIGNATURA: Programación para Lenguajes Estructurados
 */
public class Disparo extends Sprite{
       
    //CONSTRUCTOR DEL DISPARO
    public Disparo() {
        this.setVelocidad(3);
        try {
            imagen = ImageIO.read(getClass().getResource("/imagenes/disparo.png"));
            //this.altoMundo = _altoMundo;
        } catch (IOException e) {
            
        }
    }
    
    //Método mueve, que mueve los disparos
    public void mueve(){
        if(this.getY() > 0){
            this.setY(this.getY() - this.getVelocidad());
        }
    }
    
    //Método posiciona, que posiciona los disparos
    //respecto de la posición de la nave
    public void posicionaDisparo (Nave n){
        this.setX(n.getX() + n.imagen.getWidth(null)/2 - this.imagen.getWidth(null)/2);
        this.setY(n.getY());
    }
}
