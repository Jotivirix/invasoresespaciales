/*
 * Clase Nave
 * En esta clase añadiremos la nave
 * mediante la cual nos moveremos
 * por el juego InvasoresEspaciales
 */
package codigo;

import java.awt.Image;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
  * AUTOR: José Ignacio Navas Sanz
 *
 * CURSO: 1º DAM
 *
 * ASIGNATURA: Programación para Lenguajes Estructurados
 */

public class Nave extends Sprite{
       
    //Declaro el ancho de la pantalla
    private int anchoMundo;
    
    //Variables booleanas del movimiento de la nave
    private boolean pulsadoIzquierda = false;
    private boolean pulsadoDerecha = false;
    
    
    
    //CONSTRUCTOR DE LA NAVE
    public Nave(int _anchoMundo) {
        this.setVelocidad(5);
        try{
            imagen = ImageIO.read(getClass().getResource("/imagenes/nave.png"));
            this.anchoMundo = _anchoMundo;
        }
        catch(IOException e){
        }
        
    }
       
    //Método del movimiento
    public void mueve(){
        /*
        Si pulsamos la tecla izquierda, y la nave
        está a la derecha del margen izquierdo de
        la aplicación la nave se moverá a la izquierda
        */
        if(this.pulsadoIzquierda && this.getX() > 0){
            this.setX (this.getX() - this.getVelocidad());
        }
        /*
        Si pulsamos la tecla derecha, y la nave 
        está a la izquierda del margen derecho 
        de la aplicación la nave se moverá a la
        derecha. La nave se moverá hasta el margen
        menos 2 píxeles. Esos 2 píxeles permiten
        que la nave quede mejor cuando se pare
        Si no pusiéramos eso, la nave quedaría justo
        en el borde, cosa que no me gusta.
        */
        if(this.pulsadoDerecha && this.getX() < this.anchoMundo-imagen.getWidth(null)-2){
            this.setX(this.getX() + this.getVelocidad());
        }
    }   
    
    //Encapsulamiento de pulsadoIzquierda y pulsadoDerecha
    public boolean isPulsadoIzquierda() {
        return pulsadoIzquierda;
    }

    public void setPulsadoIzquierda(boolean pulsadoIzquierda) {
        this.pulsadoIzquierda = pulsadoIzquierda;
    }

    public boolean isPulsadoDerecha() {
        return pulsadoDerecha;
    }

    public void setPulsadoDerecha(boolean pulsadoDerecha) {
        this.pulsadoDerecha = pulsadoDerecha;
    }
}  